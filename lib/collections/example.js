/***************************************************************/
/***************************************************************/
/*  Example
/***************************************************************/
/***************************************************************/

// Example = new Mongo.Collection('example');

// //Allow
// Example.allow({
//   insert: function(userId, doc) {},
//   update: function(userId, doc, fields, modifier) {},
//   remove: function(userId, doc) {},
//   fetch: ['owner'],
//   transform: function() {}
// });

// //Deny
// Example.deny({
//   insert: function(userId, doc) {},
//   update: function(userId, doc, fields, modifier) {},
//   remove: function(userId, doc) {},
//   fetch: ['locked'],
//   transform: function() {}
// });
