/***************************************************************/
/* Config Routes */
/***************************************************************/

/*******************************/
/* Global Config */
/*******************************/

Router.configure({
  layoutTemplate: 'LayoutDefault',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'notFound',
});


/*******************************/
/* Index Route */
/*******************************/

Router.route('/', {
  name: 'index.link',
  template: 'index',
  controller: 'index_Controller'
});


/***************************************************************/
/* Public Routes */
/***************************************************************/

Router.route('/register', {
  name: 'register.link',
  template: 'register',
  controller: 'register_Controller'
});

Router.route('/login', {
  name: 'login.link',
  template: 'login',
  controller: 'login_Controller'
});

Router.route('recover-password', {
  path: '/recover-password',
  template: 'recoverPassword',
  onBeforeAction: function() {
    Session.set('currentRoute', 'recover-password');
    return this.next();
  }
});

Router.route('reset-password', {
  path: '/reset-password/:token',
  template: 'resetPassword',
  onBeforeAction: function() {
    Session.set('currentRoute', 'reset-password');
    Session.set('resetPasswordToken', this.params.token);
    return this.next();
  }
});


/***************************************************************/
/* Private Routes */
/***************************************************************/