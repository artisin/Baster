/***************************************************************/
/***************************************************************/
/*  UI Helpers
/*  Define UI helpers for common template functionality.
/***************************************************************/
/***************************************************************/

/*Current Route
Return an active class if the currentRoute session variable name
(set in the appropriate file in /client/routes/) is equal to the name passed
to the helper in the template*/
UI.registerHelper('currentRoute', function(route) {
  if (Session.equals('currentRoute', route)) {
    return 'active';
  } else {
    return '';
  }
});


/*******************************/
/* Error Label Alert
/* This will append an error class and label to an ipunt
/*******************************/

inputError = function (element, message) {
  //Add an error class to the element
  element.parent().addClass("error");
  //Calculate error label width
  var labelWidth = element.outerWidth();
  //Append error message to the label
  element.parent().append("<label class='error'>"+message+"</label>");
  //Set error label width
  element.find('label').css('width', labelWidth);

  //Will focus on error input after timeout
  setTimeout(function () {
    element.focus();
  }, 600);

  //Once user keydown this will clean up the error
  element.keydown(function(){
    //Fade out error label
    element.parent().find('label').fadeOut(400, function() {
      this.remove();
    });
    //Remove error class
    setTimeout(function(){
      element.parent().removeClass('error');
    }, 425); 
  });
}