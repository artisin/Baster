/***************************************************************/
/***************************************************************/
/*  Controller: Reset Password
/*  Template: /client/views/public/reset-password.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/

/*******************************/
/* Tmpl Created */
/*******************************/
Template.resetPassword.created = function() {};

/*******************************/
/* Tmpl Rendered */
/*******************************/
Template.resetPassword.rendered = function() {
  return $('#resetPassword_Form').validate({
    rules: {
      newPassword: {
        required: true,
        minlength: 6
      },
      repeatNewPassword: {
        required: true,
        minlength: 6,
        equalTo: "[name='newPassword']"
      }
    },
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      newPassword: {
        required: "Please enter a new password.",
        minlength: "Password must be at least six characters."
      },
      repeatNewPassword: {
        required: "Please repeat your new password.",
        equalTo: "Your password does not match."
      }
    },
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.resetPassword.helpers({});

/***************************************************************/
/* Events */
/***************************************************************/
Template.resetPassword.events({
  'submit #resetPassword_Form': function(e, t) {
      //Grabs the users reset token and new password
      var token = Session.get('resetPasswordToken');
      var newPassword = t.find('#newPassword').value;
      //Resets the user's password
      Accounts.resetPassword(token, newPassword, function(error) {
        if (error){
           ErrorMessage.insert({errormessage: error.reason});
        }
      });
    return false; 
    }
});