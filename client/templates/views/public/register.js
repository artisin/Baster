/***************************************************************/
/***************************************************************/
/*  Controller: register
/*  Template: /client/views/public/register.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Tmpl Created */
/*******************************/
Template.register.created = function() {};

/*******************************/
/* Tmpl Rendered */
/*******************************/
Template.register.rendered = function() {
  return $('#register_Form').validate({
    rules: {
      username: {
        required: true,
        minlength: 3,
        maxlength: 15,
      },
      emailAddress: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      },
      cpassword: { 
        required: true, 
        equalTo: "#password", 
        minlength: 6
      }, 
    },
    focusCleanup: true,
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      username: {
        required: "Please enter a username to sign up",
        minlength: "Please use at least three characters.",
        maxlength: "You username cannot be 15 characters or longer"
      },
      emailAddress: {
        required: "Please enter your email address.",
        email: "Please enter a valid email address."
      },
      password: {
        required: "Please enter a password to sign up.",
        minlength: "Please use at least six characters."
      },
      cpassword: {
        required: "Please enter a password to sign up.",
        minlength: "Please use at least six characters.",
        equalTo: "Please enter matching passwords."
      }
    },
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.register.helpers({});

/***************************************************************/
/* Events */
/***************************************************************/
Template.register.events({
 'submit #register_Form': function (e, tmpl) {
    e.preventDefault();
    //Grab the user reg data
    var registerName = tmpl.find('[name="username"]').value.trim();
    var registerEmail = tmpl.find('[name="emailAddress"]').value.trim();
    var registerPassword = tmpl.find('[name="password"]').value;
    //Put data into obj
    var newUser = {
        username: registerName,
        email: registerEmail,
        password: registerPassword};

    //Added user, checked by shcema
    Accounts.createUser(newUser, function (error) {
      if (error) {

        //If register credentials exsits throw error on label and focus
        if (error.reason === "Username already exists.") {
          inputError($('.register-username'), error.reason);}
        if(error.reason === "Email already exists."){
          inputError($('.register-email'), error.reason);}

        //Error popup message
        ErrorMessage.insert({errormessage: error.reason});
      }
    });
  }
});