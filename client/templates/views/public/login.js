/***************************************************************/
/***************************************************************/
/*  Controller: Login
/*  Template: /client/views/public/login.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Tmpl Created */
/*******************************/
Template.login.created = function() {};

/*******************************/
/* Tmpl Rendered */
/*******************************/
Template.login.rendered = function() {
  return $('#application-login').validate({
    rules: {
      logincredential: {
        required: true,
      },
      password: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      logincredential: {
        required: "Please enter your username or email address to login."
      },
      password: {
        required: "Please enter your password to login."
      },
    },
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.login.helpers({
  example: function() {}
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.login.events({
  'submit #application-login': function(e, tmpl) {
    e.preventDefault();

    var loginCredential= tmpl.find('[name="logincredential"]').value.trim(),
        userPassword = tmpl.find('[name="password"]').value,
        filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        userCredential = null;

    //Determins if user iputs username or emial
    if (filter.test(loginCredential)) {
      userCredential = loginCredential;
    }else{
      userCredential = loginCredential;
    }


    return Meteor.loginWithPassword(userCredential, userPassword, function(error) {
      if (error) {
        
        //If statments append error class and label to input if error
        if (error.reason === "User not found") {
          inputError($('.input-username'), error.reason);}
        if (error.reason === "Incorrect password") {
          inputError($('.input-password'), error.reason);};

        //Error popup message
        ErrorMessage.insert({errormessage: error.reason});
      }
    });
  },
});
