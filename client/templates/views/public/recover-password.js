/***************************************************************/
/***************************************************************/
/*  Controller: Recover Password
/*  Template: /client/views/public/recover-password.html
/***************************************************************/
/***************************************************************/
// var userError = new ReactiveVar(false);

/***************************************************************/
/* Template States */
/***************************************************************/

Template.recoverPassword.rendered = function() {
  //Basic validation
  return $('#recoverPassword').validate({
    rules: {
      emailAddress: {
        required: true,
        email: true
      }
    },
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      emailAddress: {
        required: "Please enter your email address",
        email: "Please enter a valid email address."
      }
    }
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/


/***************************************************************/
/* Events */
/***************************************************************/
Template.recoverPassword.events({
  'submit #recoverPassword': function(e, tmpl) {
    e.preventDefault();
    //Grab the user email
    var email = tmpl.find('[name="emailAddress"]').value.trim()
    return Accounts.forgotPassword({
        email: email
      }, function(error) {
        if (error) {
          inputError($('#recoverP_emialInput'), error.reason)
          //Create a error in local collection
          ErrorMessage.insert({errormessage: error.reason})
        }else{
          //If Success, fadeout and hide display
          $('#recoverP_Container').fadeOut(400, function () {
            setTimeout(function () {
              $('#recoverP_Container').hide();
              //Call show function
              showSuccess();
            }, 450)
          });
        }
      });
      //Tells user that the proccess has been succsesful
      function showSuccess () {
        console.log('called')
        $('#recoverP_Success').show();
        $('#recoverP_Success').addClass('fadeIn');
      }
  }
});